package lkqd.lkqdad;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.moat.analytics.mobile.lqd.WebAdTracker;
import com.moat.analytics.mobile.lqd.MoatFactory;

/**
 * Created by nevans on 10/23/16.
 */
public class LKQDAD
{
    //webview that will be created
    private WebView _webView;

    //moat ad tracker is used by moat ad services for analytics and tracking
    private WebAdTracker _moatAdTracker;

    //reference to activity that will be handling webview and other actions
    private Activity _activity;

    //handles events from webview
    private LKQDWebViewClient _webViewClient;

    //the class that will listen for events from lkqdad
    private LKQDDelegate _LkqdDelegate;

    //pid and sid are custom values based on lkqd account
    private int _pid = 0;
    private int _sid = 0;

    //how much control the user has over the ad
    private int _automationLevel = 1;

    //bitrate of video for automation levels 1 or 2
    private double _desiredBitRate = 600;

    //keep tabs on which state has been hit so it can't be called multiple times
    private boolean _adLoadedCalled = false;
    private boolean _startAdCalled = false;
    private boolean _initAdCalled = false;

    //dimensions of webview
    private int _screenWidth = 0;
    private int _screenHeight = 0;

    //dimensions of video
    private Number _videoWidth;
    private Number _videoHeight;

    //vpaid values
    private double _adDuration = -2;
    private double _adRemainingTime = -1;
    private double _adWidth = -1;
    private double _adHeight = -1;
    private double _adVolume = -1;
    private boolean _adExpanded = false;
    private boolean _adLinear = false;
    private boolean _adSkippableState = false;
    private boolean _adIcons = false;
    private String _adCompanions = "";
    private String _handshakeVersion = "2.0";

    //store layout for resizing
    private FrameLayout.LayoutParams _layoutParams = new FrameLayout.LayoutParams(_screenWidth,_screenHeight);

    //time out time for the ad to get adLoaded event back once initAd() has been called
    private long _adLoadTimeoutTime = 10000; //time in ms for ad timeout

    //timers for init ad timeout and delayed initAd() call
    Handler _initTimeoutHandler = new android.os.Handler();
    Runnable _timeoutInitAd = new Runnable() {
        public void run()
        {
            handleAdTimeout();
        }
    };

    //location values
    private double _longitude;
    private double _latitude;
    private boolean _locationFound = false;

    // Acquire a reference to the system Location Manager
    LocationManager _locationManager;

    // Define a listener that responds to location updates
    LocationListener _locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // Called when a new location is found by the network location provider.
            _latitude = location.getLatitude();
            _longitude = location.getLongitude();

            if (ContextCompat.checkSelfPermission(_activity, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(_activity, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                            PackageManager.PERMISSION_GRANTED) {
                _locationManager.removeUpdates(_locationListener);
            }

            makeWebViewRequest();
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    //constructor for lkqd ad
    public LKQDAD(Activity activity, LKQDDelegate lkqdDelegate, int pid, int sid, int automationLevel, double desiredBitRate)
    {
        _pid = pid;
        _sid = sid;
        _automationLevel = automationLevel;
        _desiredBitRate = desiredBitRate;
        _activity = activity;
        _LkqdDelegate = lkqdDelegate;
        _screenWidth = 0;
        _screenHeight = 0;

        _moatAdTracker = new WebAdTracker() {
            @Override
            public boolean track() {
                return true;
            }
        };

        DisplayMetrics dm = new DisplayMetrics();
        _activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        _screenWidth = dm.widthPixels;
        _screenHeight = dm.heightPixels;

        Configuration configuration = _activity.getResources().getConfiguration();
        _videoWidth = configuration.screenWidthDp;
        _videoHeight = configuration.screenHeightDp;

        turnOnLocationServices();
    }

    public void turnOnLocationServices()
    {
        //System.out.println("checking location services");

        //device level check on location services  enabled
        if(isLocationEnabled(_activity))
        {
            //System.out.println("ENABLED location services on device");

            //program level check
            //need to have permissions like ACCESS_FINE_LOCATION turned on in the manifest.xml
            if (ContextCompat.checkSelfPermission(_activity, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(_activity, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                            PackageManager.PERMISSION_GRANTED) {

                //System.out.println("ENABLED location services in app");
                _locationManager = (LocationManager) _activity.getSystemService(Context.LOCATION_SERVICE);
                _locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, _locationListener);
            } else {
                //System.out.println("DISABLED location services in app");
                _locationFound = false;
                makeWebViewRequest();
            }
        }
        else
        {
            //System.out.println("DISABLED location services on device");
            _locationFound = false;
            makeWebViewRequest();
        }
    }

    //checks if the device has the location settings turned on
    private static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    //this starts the webview loading and lkqd.html request
    public void makeWebViewRequest()
    {
        PackageManager packageManager = _activity.getPackageManager();
        ApplicationInfo applicationInfo = null;
        try
        {
            applicationInfo = packageManager.getApplicationInfo(_activity.getApplicationInfo().packageName, 0);
        }
        catch (final PackageManager.NameNotFoundException e)
        {
        }

        String appName = "";

        if(applicationInfo != null)
        {
            appName = (String)packageManager.getApplicationLabel(applicationInfo);
        }

        PackageManager manager = _activity.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = manager.getPackageInfo(
                    _activity.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String versionNumber = "";

        if(packageInfo != null) {
            versionNumber = packageInfo.versionName;
        }

        //String appName = _activity.getApplicationInfo().loadLabel(_activity.getPackageManager()).toString();
        System.out.println("appName " + appName);
        System.out.println("versionNumber " + versionNumber);

        String bundleID = BuildConfig.APPLICATION_ID;
        System.out.println("bundleID " + bundleID);

        if(_locationFound)
        {
            System.out.println("latitude " + Double.valueOf(_latitude));
            System.out.println("longitude " + Double.valueOf(_longitude));
        }
        else
        {
            System.out.println("latitude n/a");
            System.out.println("longitude n/a");
        }

        String strIDFA = Settings.Secure.getString(_activity.getContentResolver(), Settings.Secure.ANDROID_ID);

        //build the query string to send app data for tracking
        String queryString = "?appname="+appName;
        queryString = queryString + "&idfa="+strIDFA;
        queryString = queryString + "&appversion="+versionNumber;
        queryString = queryString + "&bundleid="+bundleID;
        queryString = queryString + "&latitude="+_latitude;
        queryString = queryString + "&longitude="+_longitude;
        queryString = queryString + "&pid="+_pid;
        queryString = queryString + "&sid="+_sid;
        queryString = queryString + "&sdk=android";

        _webView = new WebView(_activity);
        _webView.setWebChromeClient(new WebChromeClient());
        _webViewClient = new LKQDWebViewClient(this);
        _webView.setWebViewClient(_webViewClient);
        _webView.getSettings().setJavaScriptEnabled(true);
        _webView.getSettings().setDomStorageEnabled(true);
        _webView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        //String urlString = "https://devflash.lkqd.com/html/lkqd.html";
        String urlString = "https://ad.lkqd.net/sdk/lkqd.html";
        urlString = urlString + queryString +"&cb="+ System.currentTimeMillis();

        _webView.loadUrl(urlString);
    }

    //called when we don't receive AdLoaded before timeout
    private void handleAdTimeout()
    {
        _initTimeoutHandler.removeCallbacks(_timeoutInitAd);

        //System.out.println("REMOVING TIMER");

        //ad has timed out so we alert the app
        _LkqdDelegate.lkqdAdTimeOut(this);
    }

    //removes the webview and alerts the app that the ad is over
    private void removeWebView()
    {
        //remove the web view
        if(_webView != null)
        {
            if(_webView.getParent() != null)
            {
                ((ViewGroup) _webView.getParent()).removeView(_webView);
            }
        }

        //alert app
        _LkqdDelegate.lkqdAdEnded(this);
    }

    //used to access activity from LKQDWebViewClient
    public Activity getActivity()
    {
        return _activity;
    }

    //called from LKQDWebViewClient to alert app of value change
    public void alertStatusChange(String valueChanged)
    {
        _LkqdDelegate.lkqdStatusChange(this, valueChanged);
    }

    //called from LKQDWebViewClient to update ad data with latest values
    public void handleUpdate(String param, String value)
    {
        boolean booleanVal;
        double doubleVal;

        switch (param)
        {
            case "adDuration":
                doubleVal = Double.valueOf(value).doubleValue();
                _adDuration = doubleVal;
                //System.out.println("adDuration = " + doubleVal);
                break;
            case "adRemainingTime":
                doubleVal = Double.valueOf(value).doubleValue();
                _adRemainingTime = doubleVal;
                //System.out.println("adRemainingTime = " + doubleVal);
                break;
            case "adWidth":
                doubleVal = Double.valueOf(value).doubleValue();
                _adWidth = doubleVal;
                //System.out.println("adWidth = " + doubleVal);
                break;
            case "adHeight":
                doubleVal = Double.valueOf(value).doubleValue();
                _adHeight = doubleVal;
                //System.out.println("adHeight = " + doubleVal);
                break;
            case "adVolume":
                doubleVal = Double.valueOf(value).doubleValue();
                _adVolume = doubleVal;
                //System.out.println("adVolume = " + doubleVal);
                break;
            case "adExpanded":
                booleanVal = Boolean.valueOf(value).booleanValue();
                _adExpanded = booleanVal;
                //System.out.println("adExpanded = " + booleanVal);
                break;
            case "adLinear":
                booleanVal = Boolean.valueOf(value).booleanValue();
                _adLinear = booleanVal;
                //System.out.println("adLinear = " + booleanVal);
                break;
            case "adSkippableState":
                booleanVal = Boolean.valueOf(value).booleanValue();
                _adSkippableState = booleanVal;
                //System.out.println("adSkippableState = " + booleanVal);
                break;
            case "adIcons":
                booleanVal = Boolean.valueOf(value).booleanValue();
                _adIcons = booleanVal;
                //System.out.println("adIcons = " + booleanVal);
                break;
            case "adCompanions":
                _adCompanions = value;
                //System.out.println("adCompanions = " + value);
                break;
            case "handshakeVersion":
                _handshakeVersion = value;
                //System.out.println("handshakeVersion = " + value);
                break;
            default:
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    //all vpaid events pass through here before being relayed to user
    //lkqdad will set anything it needs to locally first
    //this also handles automation based on events
    public void handleVpaidEvent(String vpaidEvent)
    {
        //System.out.println("handleVpaidEvent in lkqdAd: " + vpaidEvent);

        //handle event locally and do whatever needed for this lkqdAd instance
        if(vpaidEvent.equals("AdStopped") || vpaidEvent.equals("AdError"))
        {
            removeWebView();
        }

        //called after initAd() has finished
        if(vpaidEvent.equals("AdLoaded"))
        {
            //remove timeout on initAd()
            _initTimeoutHandler.removeCallbacks(_timeoutInitAd);
            //once this is true, we can start setting values on the vpaid ad unit
            _adLoadedCalled = true;
        }

        //this should be called from javascript when the orientation changes
        //resizes the webview frame to the new view controller frame width/height
        if(vpaidEvent.equals("AdSizeChange"))
        {
            //updates stored values for screen width/height
            DisplayMetrics dm = new DisplayMetrics();
            _activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
            _screenWidth = dm.widthPixels;
            _screenHeight = dm.heightPixels;

            //updates video width/height for automated ads
            Configuration configuration = _activity.getResources().getConfiguration();
            _videoWidth = configuration.screenWidthDp;
            _videoHeight = configuration.screenHeightDp;

            //sets webview layout to new screen width/height values
            _layoutParams.height = _screenHeight;
            _layoutParams.width = _screenWidth;
            _webView.setLayoutParams(_layoutParams);
        }

        //turn on Moat analytics once the page has loaded
        if(vpaidEvent.equals("webViewLoaded"))
        {
            //turn on moat once the webview is loaded
            _moatAdTracker = MoatFactory.create(_activity).createWebAdTracker(_webView);
            boolean moatOK = _moatAdTracker.track();
            System.out.println("MOAT OK = ");
            System.out.println(moatOK);
        }

        /*
         Automation Level handling here
         If automationLevel != 0, then this class will handle some of the vpaid events
         Default is 1 which means the user will only have to call startAd() once they receive AdLoaded event
         Level 2 means the ad will automatically load and start ASAP if nothing fails
         Level 0 would be the user controlling each step and responding to webViewLoaded and AdLoaded themselves
        */

        //automationLevel=1
        //will do everything but call startAd() for the user
        if(_automationLevel > 0)
        {
            if(vpaidEvent.equals("webViewLoaded"))
            {
                autoInit();
            }
        }

        //automationLevel=2
        //will play the ad as soon as possible
        if(_automationLevel > 1)
        {
            if(vpaidEvent.equals("AdLoaded"))
            {
                System.out.println("AdLoaded AUTO START");
                startAd();
            }
        }

        //LKQDAD has done all it needs to with the event...
        //pass the event on to the delegate to handle how it wants
        _LkqdDelegate.lkqdEventReceived(this, vpaidEvent);
    }

    //called from automation level 1 and 2
    //delay is required due to javascript threading on android
    private void autoInit()
    {
        //System.out.println("webViewLoaded AUTO START");

        //create the initad() command
        String command = "javascript:initAd('"+_videoWidth+"','"+_videoHeight+"','"+"normal"+"','"+_desiredBitRate+"','','')";
        //System.out.println(command);
        _webView.loadUrl(command);
    }

    //useful check for automation level 0 and 1 rather than having to store it on the app
    public boolean isAdLoaded()
    {
        return _adLoadedCalled;
    }

    /*
        //////////////////////VPAID interface implementation////////////////////////
        The following functions are added just to maintain vpaid protocol.
        A lot will not be necessary.
        All functions and their use cases can be found here:
        https://www.iab.com/wp-content/uploads/2015/06/VPAID_2_0_Final_04-10-2012.pdf
    */
    public void initAd(Number width, Number height, String viewMode, Number desiredBitrate, String creativeData, String environmentVars)
    {
        //prevent user from calling initAd multiple times
        if(_initAdCalled == false)
        {
            _initAdCalled = true;
        }
        else
        {
            return;
        }

        String command = "javascript:initAd('"+width+"','"+height+"','"+viewMode+"','"+desiredBitrate+"','"+creativeData+"','"+environmentVars+"')";
        //System.out.println(command);
        _webView.loadUrl(command);

        //just in case init ad doesn't complete start a timer here for timing out initAd() call
        _initTimeoutHandler.removeCallbacks(_timeoutInitAd);
        _initTimeoutHandler.postDelayed(_timeoutInitAd, _adLoadTimeoutTime);
    }

    //this starts the ad and also puts the webview on screen
    public void startAd()
    {
        //prevent user from calling startAd multiple times
        if(_startAdCalled == false)
        {
            _startAdCalled = true;
        }
        else
        {
            return;
        }

        //show the webview
        _layoutParams.width = _screenWidth;
        _layoutParams.height = _screenHeight;
        _activity.addContentView(_webView,_layoutParams);

        //start the ad
        String command = "javascript:startAd()";
        _webView.loadUrl(command);
    }

    public void resizeAd(Number width, Number height, String viewMode)
    {
        //should only be calling resize ad once we know the ad is loaded
        if(_adLoadedCalled)
        {
            String command = "javascript:resizeAd('"+width+"','"+height+"','"+viewMode+"')";
            _webView.loadUrl(command);
        }
    }

    public void stopAd()
    {
        String command = "javascript:stopAd()";
        _webView.loadUrl(command);
    }

    public void pauseAd()
    {
        String command = "javascript:pauseAd()";
        _webView.loadUrl(command);
    }

    public void resumeAd()
    {
        String command = "javascript:resumeAd()";
        _webView.loadUrl(command);
    }

    public void expandAd()
    {
        String command = "javascript:expandAd()";
        _webView.loadUrl(command);
    }

    public void collapseAd()
    {
        String command = "javascript:collapseAd()";
        _webView.loadUrl(command);
    }

    public void skipAd()
    {
        String command = "javascript:skipAd()";
        _webView.loadUrl(command);
    }

    public boolean getAdLinear()
    {
        return _adLinear;
    }

    public boolean getAdExpanded()
    {
        return _adExpanded;
    }

    public Number getAdRemainingTime()
    {
        return _adRemainingTime;
    }

    public Number getAdVolume()
    {
        return _adVolume;
    }

    //value from 0-1 with 0 being mute and 1 being 100% volume
    public void setAdVolume(Number value)
    {
        String command = "javascript:setAdVolume('"+value+"')";
        _webView.loadUrl(command);
    }

    //typically send "2.0" although this is already done on webview loading, anyway
    public void handshakeVersion(String playerVPAIDVersion)
    {
        String command = "javascript:handshakeVersion('"+playerVPAIDVersion+"')";
        _webView.loadUrl(command);
    }

    public String getHandshakeVersion()
    {
        return _handshakeVersion;
    }

    //width of ad usually an integer like 320
    public Number getAdWidth()
    {
        return _adWidth;
    }

    //height of ad usually an integer like 480
    public Number getAdHeight()
    {
        return _adHeight;
    }

    //whether or not an ad is skippable
    public boolean getAdSkippableState()
    {
        return _adSkippableState;
    }

    //total length of time for the ad (15.19 seconds for example)
    public Number getAdDuration()
    {
        return _adDuration;
    }

    public String getAdCompanions()
    {
        return _adCompanions;
    }

    public boolean getAdIcons()
    {
        return _adIcons;
    }
}
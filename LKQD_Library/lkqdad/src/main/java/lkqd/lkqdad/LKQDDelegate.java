package lkqd.lkqdad;

/**
 * Created by nevans on 10/23/16.
 */
public interface LKQDDelegate
{
    public void lkqdAdEnded(LKQDAD lkqdAd);
    public void lkqdEventReceived(LKQDAD lkqdAd, String eventString);
    public void lkqdAdTimeOut(LKQDAD lkqdAd);
    public void lkqdStatusChange(LKQDAD lkqdAd, String value);
    //need an event for more than just timeout...like an error on the lkqd.html if vpaidunit is null
}
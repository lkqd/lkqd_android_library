package lkqd.lkqdad;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by nevans on 10/23/16.
 */
public class LKQDWebViewClient extends WebViewClient
{
    private LKQDAD _lkqdAd;

    public LKQDWebViewClient(LKQDAD lkqdAd)
    {
        _lkqdAd = lkqdAd;
    }

    @Override public boolean shouldOverrideUrlLoading(WebView view, String url)
    {
        //System.out.println("checking for override");

        Uri uri = Uri.parse(url);
        String scheme = uri.getScheme();
        String query;
        String[] queries;
        String[] values;
        String strValue = "";
        String vpaidEvent = "NO_VPAID_EVENT_FOUND";

        if(scheme.equals("vpaid"))
        {
            //System.out.println("statusUpdate!!!");
            //System.out.println(uri.toString());
            query = uri.getQuery();
            queries = query.split("&");
            //System.out.println(query);
            //System.out.println("queries.length = " + queries.length);
            for(int i=0;i<queries.length;i++)
            {
                //don't send updates if the query has an empty value
                //i.e. ?x=10&y=&z=30 ... y will not have length of 2
                values = queries[i].split("=");
                if(values.length == 2)
                {
                    strValue = values[1];

                    //only vpaid events should have this value string
                    if(strValue.equals("VPAID_EVENT"))
                    {
                        vpaidEvent = values[0];
                    }
                    else //otherwise it is a related value being passed along with event
                    {
                        //this will update the value on lkqdad instance
                        _lkqdAd.handleUpdate(values[0], values[1]);

                        //this tells lkqdad instance to alert its delegate the value has changed
                        _lkqdAd.alertStatusChange(values[0]);
                    }
                }
            }

            //alert the event info AFTER the values passed with it have been handled
            if(!vpaidEvent.equals("NO_VPAID_EVENT_FOUND"))
            {
                _lkqdAd.handleVpaidEvent(vpaidEvent);
            }

            //returning true stops webview from attempting to load the request
            return true;
        }

        if(url.contains("lkqd.html") || url.contains("about:blank"))
        {
            //these should be the only requests that aren't ad click thru
        }
        else
        {
            //open the ad url in chrome
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setPackage("com.android.chrome");
            try {
                _lkqdAd.getActivity().startActivity(intent);
                //do not let the webview load it, the new activity will
                return true;
            } catch (ActivityNotFoundException e) {
                // Chrome is probably not installed
                // Try with the default browser
                intent.setPackage(null);
                _lkqdAd.getActivity().startActivity(intent);

                //do not let the webview load it, the new activity will
                return true;
            }
        }

        //do not override, let the webview load the request
        return false;
    }

    @Override public void onLoadResource(WebView view, String url)
    {
        //debugging what is currently loading
        //System.out.println("LOADING: " +url);
    }
}

